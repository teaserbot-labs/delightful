<div align="center">
  <img src="https://codeberg.org/teaserbot-labs/delightful/raw/branch/main/assets/delightful-logo.png" alt="delightful"/>
</div>

<br/><br/>

<p align="center">
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delightful.md">What is a delightful list?</a></b>&nbsp;&nbsp;&nbsp;
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md">Creating a list</a></b>&nbsp;&nbsp;&nbsp;
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-checks.md">Quality checklist</a></b>&nbsp;&nbsp;&nbsp;
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/faq.md">FAQ</a></b>
</p>

<br/>

---

<br/>

## Badge of honor

Please [delight us](delight-us.md) by launching your own curated list, and proudly wear this badge:&nbsp;&nbsp; ![delightful badge](assets/delightful-badge.png)

> **So collect your gems :gem: and fill those lists!**

## Contents

- [Delightful lists](#delightful-lists)

More information about this project:

- [A delightful tease](#a-delightful-tease)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Delightful lists

- [ActivityPub developer resources](https://codeberg.org/fediverse/delightful-activitypub-development/) - Frameworks, libraries, utitilites and specifications to build the Fediverse.
- [C software](https://codeberg.org/lmemsm/delightful-c-software) - Lightweight, cross-platform C programs and libraries.
- [C4 software](https://codeberg.org/IgnisIncendio/delightful-c4/src/branch/main/README.md) - Free software projects that follow the Collective Code Construction Contract (C4).
- [CLI software](https://codeberg.org/chaihron/delightful-cli/src/main/README.md) - CLI software for the console lovers.
- [Creative tools](https://codeberg.org/ADHDefy/delightful-creative-tools) - Tools for digital creatives in a variety of mediums.
- [Databases](https://codeberg.org/yarmo/delightful-databases/src/branch/main/README.md) - Databases in all shapes and sizes.
- [Educational games](https://codeberg.org/lmemsm/delightful-educational-games) - Free, Libre and Open Source educational games.
- [Fediverse apps](https://codeberg.org/fediverse/delightful-fediverse-apps) - All known server applications of the ActivityPub Fediverse.
- [Fediverse clients](https://codeberg.org/fediverse/delightful-fediverse-clients) - Web, desktop and mobile clients to access the ActivityPub Fediverse.
- [Forgejo](https://codeberg.org/forgejo-contrib/delightful-forgejo/) - Tools and resources related to the [Forgejo](https://forgejo.org/) FLOSS software forge.
- [Funding](https://codeberg.org/teaserbot-labs/delightful-funding/src/branch/main/README.md) - Funding FOSS, Open Data, Open Science projects and sustainable businesses.
- [Gamification](https://codeberg.org/teaserbot-labs/delightful-gamification/src/branch/main/README.md) - Gamification and reputation system resources for developers.
- [Humane AI](https://codeberg.org/teaserbot-labs/delightful-small-ai/src/branch/main/README.md) - Humane artificial intelligence resources that are open and accessible.
- [Humane design](https://codeberg.org/teaserbot-labs/delightful-humane-design/src/branch/main/README.md) - Humane design resources for UX designers and developers.
- [Libre hosters](https://codeberg.org/jonatasbaldin/delightful-libre-hosters) - People and organizations who deploy, maintain and offer open source services to the public.
- [Linked data](https://codeberg.org/teaserbot-labs/delightful-linked-data/src/branch/main/README.md) - Linked data resources for developers.
- [Mathematics softwares](https://codeberg.org/mskf1383/delightful-mathematics-softwares/src/branch/main/README.md) - Free, Libre and OpenSource softwares for mathematics!
- [Matrix](https://codeberg.org/yarmo/delightful-matrix) - Matrix resources, implementations and clients.
- [Off-grid Open tech](https://codeberg.org/lmemsm/delightful-off-grid-open-tech) - Technology, Open hardware and Open Source software resources for use off-grid, for power outages and other emergency situations.
- [Open science](https://codeberg.org/teaserbot-labs/delightful-open-science/src/branch/main/README.md) - Resources, organization and free software that support open science.
- [Sustainable business](https://codeberg.org/teaserbot-labs/delightful-sustainable-business/src/branch/main/README.md) - Sustainable business resources and exemplars of doing business online.
- [Sustainable VPS](https://codeberg.org/jonatasbaldin/delightful-sustainable-vps) - Sustainable VPS (Virtual Private Servers) providers.
- [Transit](https://codeberg.org/Pi-Cla/delightful-transit) - FOSS transit APIs, apps, datasets, research, and software

## About this project

This repository sits in the organization [teaserbot labs™](https://codeberg.org/teaserbot-labs). It is not a company. The name is just a tease. The goal of this project is to put a spotlight on some great open alternatives to proprietary works, that exist in the areas of FOSS, Open Data and Open Science. More information is available in our [FAQ](faq.md#about-social-coding-movement).

## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/yarmo/delightful-databases/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [Arnold Schrijver](https://socialhub.activitypub.rocks/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@smallcircles@social.coop](https://social.coop/@smallcircles))
- [Houkime](https://notabug.org/houkime) (codeberg: [@houkime](https://codeberg.org/houkime), fediverse: [@houkimenator@koyu.space](https://koyu.space/@houkimenator))

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, the [maintainers](#maintainers) and other [contributors](delightful-contributors.md) have waived all copyright and related or neighboring rights to this work.

Many thanks to Sindre Sorhus for some [awesome](delightful.md#inspired-by-awesome-lists) inspiration, and to [Codeberg](https://codeberg.org) for providing us this space.
