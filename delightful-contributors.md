# delightful contributors

These fine people brought us delight by adding their gems of freedom to the delight project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](delight-us.md#attribution-of-contributors).

## We thank you for your gems of freedom :gem:

- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))
- [Yarmo Mackenbach](https://yarmo.eu/) (codeberg: [@yarmo](https://codeberg.org/yarmo), fediverse: [@yarmo@fosstodon.org](https://fosstodon.org/@yarmo))
- Chaihron (codeberg: [@chaihron](https://codeberg.org/chaihron), fediverse: [@chaihron@nixnet.social](https://nixnet.social/chaihron)
- [Ignis Incendio](https://IgnisIncendio.me/) (Fediverse: [@ignis@poketopia.city](https://poketopia.city/@ignis))
- [Caesar Schinas](https://caesarschinas.com/) (codeberg: [@caesar](https://codeberg.org/caesar), fediverse: [@caesar@indieweb.social](https://indieweb.social/@caesar))
- [Pi-Cla](https://pi-cla.xyz) (codeberg: [@Pi-Cla](https://codeberg.org/Pi-Cla), fediverse: [@PiCla@fosstodon.org](https://fosstodon.org/@PiCla))
