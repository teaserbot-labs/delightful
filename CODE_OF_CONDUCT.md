# Community Participation Guidelines

This repository is governed by Social Coding Movement's code of conduct and etiquette guidelines. By your interaction you accept to abide by its rules.

For more details, please read the [Social Coding Community Participation Guidelines](https://participate.coding.social). 

## How to Report

For more information on how to report violations of the Community Participation Guidelines, please read the '[Reporting](https://participate.coding.social/#reporting)' section.